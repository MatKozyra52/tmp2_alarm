/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file alarm.h
 * @author MatKozyra
 * @date Dec 2020
 * @brief main library header for Alarm project
 * @ver 0.1
 */

#ifndef ALARM_H
#define ALARM_H

#include "KL05Zutil.h"
#include "uart.h"

#define passlen 4				//password length
#define ESCAPE_TIME 10   //time to exit 
#define DEFUZE_TIME 10   //time to defuze
#define INPUT_SENSOR 2   //number of input sensors
#define DOORS 6
#define MOVE 7
#define BUZZER_PIN 11

static uint8_t pass[passlen];  //password holder

//STATES
typedef enum {
	INIT,
	WAITING,
	ESCAPE,
	GUARD,
	DEFUZE,
	ALARM
} Alarm_State;

static Alarm_State mState;

/**
 * @brief Change machine's state and its functionality
 * @param State
 * @return Next state name
 */
Alarm_State SetState(Alarm_State);

/**
 * @brief Inicjalization -- mainly ports and variables
 */
void Alarm_Init(void);

/**
 * @brief Run Systick  -- alarm sound
 */
void Alarm_Start(uint32_t freq);

/**
 * @brief Run Systick -- alarm sound
 */
void Alarm_Stop(void);

/**
 * @brief Set password
 * @param Pointer to array with password
 */
void SetPass(uint8_t *buff);

/**
 * @brief Check password
 * @param Password to check
 * @return Is password correct?
 */
bool CheckPass(uint8_t *buff);


#endif //ALARM_H
