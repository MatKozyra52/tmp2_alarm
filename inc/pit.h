/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file pit.h
 * @author MatKozyra
 * @date Dec 2020
 * @brief library for PIT functionality
 * @ver 0.1
 */

#ifndef PIT_H
 #define PIT_H
 
# include "KL05Zutil.h"


/**
 * @brief Initialize PIT
 * @param Chanel numper and corresponding period
 */
void pit_set(uint32_t period, uint8_t channel);

/**
 * @brief Start PIT
 * @param Channel number
 */
void startPIT(uint8_t channel);

/**
 * @brief Stop PIT
 * @param Channel number
 */
void stopPIT(uint8_t channel);

#endif //PIT_H

