/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file KL05Zutil.h
 * @author MatKozyra
 * @date Dec 2020
 * @brief short utils container
 * @ver 0.1
 */
 
#ifndef KL05ZUTIL_H
#define KL05ZUTIL_H

#define FRDM_KL05Z   //used platform

#ifdef FRDM_KL05Z
# include "MKL05Z4.h"
#include "led.h"
#endif //FRDM_KL05Z


#define MASK(x)		 (uint32_t)(1UL << (x))

typedef enum { false, true } bool;


/**
 * @brief Pin info
 */
typedef struct {
	GPIO_Type 	*gpio;        /* GPIO base pointer */
	PORT_Type 	*port;        /* PORT base pointer */
	uint32_t		clk_mask;     /* Mask for SCGC5 register */
	uint8_t  		pin;          /* Number of PIN */
} PinStruct_Type;

void delay_ms( int n);	
void delay_us( int n);	

#endif
