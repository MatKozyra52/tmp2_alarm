/******************************************************************************
 * This file is a part of the SM2 Course Homework                               *                                                 *
 ******************************************************************************/

/**
 * @file button_matrix.h
 * @author Kozyra
 * @date Oct 2020
 * @brief library .h file
 * @connecting R1-R4 -> PTA8-PTA5;   C1-C4 -> PTA12-PTA9
 * @ver 1.1
 * @update interrupt functionality
 */
 
 
#ifndef button_mstrix_H
#define button_mstrix_H

#include "KL05Zutil.h"

/**
 * @brief Global structure
 */
typedef struct {
	uint8_t value;						/*Buffor for current value*/
	uint8_t prevValue;				/*Last used value*/
	bool writeIt;							/*Value has changed and it is ready to use*/
}ButtonsStruct;

static ButtonsStruct buttonstruct;  /*Global structure*/

/**
 * @brief Buttons matrix initialization
 */
void Buttons_Init(void);


/**
 * @brief Interrupt procedure
 * @param Our structure for all data
 */
void machineHandle(ButtonsStruct *);
 

#endif
