/******************************************************************************
 * This file is a part of the SM2 Project                             *
 ******************************************************************************/

/**
 * @file led.h
 * @brief File containing enums, structures and declarations for LEDs.
 * @ver 0.1
 */
 #ifndef LED_H
 #define LED_H
 
# include "KL05Zutil.h"

/*
 * @brief Led State Types.
 */
typedef enum {
	LED_OFF,
	LED_ON,
	LED_TOGGLE
} LedState_Type;
/**
 * @brief Led Color Types.
 */
typedef enum {
	LED_RED,
	LED_GREEN,
	LED_BLUE
} LedColor_Type;

/**
 * @brief LEDs initialization.
 */
void LED_Init (void);

/**
 * @brief Control choosen LED.
 *
 * @param Color of a LED.
 * @param State of a LED.
 */
void LED_Ctrl(LedColor_Type color, LedState_Type led_state);


#endif /* LED_H */
