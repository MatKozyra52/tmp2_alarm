#include <Arduino.h>
#include <FS.h>                       //internal file system
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>                  //to menage UDP

const char *ssid = "YOUR WIFI SSID";  //name
const char *pass = "YOUR WIFI PASSWORD";       //pass

ESP8266WebServer server(80);          //port 80 - browset port for easy display
WiFiUDP UDP;                          //UDP instance

IPAddress timeServerIP;
const char *NTPServerName = "time.nist.gov"; // time.nist.gov NTP server address

const int NTP_PACKET_SIZE = 48;       // packet size
byte NTPBuffer[NTP_PACKET_SIZE];      // buffer to hold incoming and outgoing packets
time_t timeUNIX = 0;                  //time value
String data;                          //output data
const char eol=0xa;                   //eol char

void handleRoot();                    //all subwebsides
void handleInit();
void handleOn(); 
void handleOff(); 
void handleAlarm(); 
void handleClear();
void handleUART();                    // changelog

void sendNTPpacket(IPAddress &address);
uint32_t getTime();

void writeToFile(String plik, String tekst);
String readFile(String plik);



void setup()
{
  Serial.begin(115200, SERIAL_8N1);    //start UART
  if (!SPIFFS.begin()) ESP.reset();    //open memory acces

  WiFi.begin(ssid, pass);              //connect

  while (WiFi.status() != WL_CONNECTED){
    delay(500);
  }

  UDP.begin(123);                      //NTP

  if (!WiFi.hostByName(NTPServerName, timeServerIP)) ESP.reset(); // Get the IP address of the NTP server

  server.on("/", handleRoot);          //what side open when we enter particular domain
  server.on("/on", handleOn);
  server.on("/off", handleOff);
  server.on("/init", handleInit);
  server.on("/alarm", handleAlarm);
  server.on("/clear", handleClear);
  server.on("/UART", handleUART);
  server.begin(); //start server
}



void loop()
{
  server.handleClient();                //waiting

  while (Serial.available() > 0)
  { //new uart value
    sendNTPpacket(timeServerIP);        // Send an NTP request

    String str = Serial.readString();    
    uint32_t time = 0;

    for (int k = 0 ; k < 20; k++){      //wait for response or wait 2sec
      time = getTime();
      delay(100);
      if(time) k=100;
    }
    timeUNIX = time + 3600;             //time shifting
    String data = ctime(&timeUNIX);     //actual time in string format
    data += str;  //data
    if (str == "Alarm") data += "\t  <i class=\"fas fa-bell\"></i>";  //alarm icon
    data += "<br>";//eol
    writeToFile("/dane.txt", data);     //save data to file
  }
}

////////////////////////////////////////////HANDLERS
void handleRoot()
{
  server.send(200, "text/html", readFile("/index.html"));
}

void handleInit()
{
  data = "Init";
  data+=eol;
  for (auto i : data){
   Serial.print(i);
   delay(50);}
  server.send(200, "text/html", readFile("/index.html"));
}

void handleOn()
{
  data = "On";
  data+=eol;
  for (auto i : data){
   Serial.print(i);
   delay(50);}
  server.send(200, "text/html", readFile("/index.html"));
}

void handleOff()
{
  data = "Off";
  data+=eol;
  for (auto i : data){
   Serial.print(i);
   delay(50);}
  server.send(200, "text/html", readFile("/index.html"));
}

void handleAlarm()
{
  data = "Alarm";
  data+=eol;
   for (auto i : data){
   Serial.print(i);
   delay(50);}
  server.send(200, "text/html", readFile("/index.html"));
}

void handleClear()
{
  SPIFFS.remove("/dane.txt");
  server.send(200, "text/html", readFile("/index.html"));
}

void handleUART()
{
  String dane = readFile("/dane.txt");      //read from file 
  server.send(200, "text/html", dane);      //and send
}

///////////////////////////////////////////////////
void writeToFile(String plik, String tekst)
{
  File file = SPIFFS.open(plik, "a");
  file.print(tekst);
  file.close();
}

String readFile(String plik)                //from SPIFFS
{
  String strona;
  File file = SPIFFS.open(plik, "r");
  while (file.available())
    strona += (char)file.read();
  file.close();
  return strona;
}

void sendNTPpacket(IPAddress &address)
{
  memset(NTPBuffer, 0, NTP_PACKET_SIZE);    // set all bytes in the buffer to 0
  NTPBuffer[0] = 0b11100011;                // Initialize values needed to form NTP request

  UDP.beginPacket(address, 123);            // send a packet requesting a timestamp:
  UDP.write(NTPBuffer, NTP_PACKET_SIZE);
  UDP.endPacket();
}

uint32_t getTime()
{
  if (UDP.parsePacket() == 0)    return 0;   // If there's no response (yet)

  UDP.read(NTPBuffer, NTP_PACKET_SIZE);      // read the packet into the buffer - Combine the 4 timestamp bytes into one 32-bit number
  uint32_t NTPTime = (NTPBuffer[40] << 24) | (NTPBuffer[41] << 16) | (NTPBuffer[42] << 8) | NTPBuffer[43];
  
  const uint32_t seventyYears = 2208988800UL;// Convert NTP time to a UNIX timestamp:     Unix time starts on Jan 1 1970. That's 2208988800 seconds in NTP time:

  uint32_t UNIXTime = NTPTime - seventyYears;   // subtract seventy years:
  return UNIXTime;
}