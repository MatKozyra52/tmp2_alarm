/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file main.c
 * @author MatKozyra
 * @date Dec 2020
 * @brief main file with complete functionality
 * @ver 0.1
 */
 
#include "inc/KL05Zutil.h"
#include "inc/button_matrix.h"
#include "inc/alarm.h"
#include "inc/pit.h"

static uint8_t counter = 0;   										//pass len counter
static uint8_t buff[passlen];
static bool pass_ready=0;
static bool button_hold=0;
static char temp;   															//UART temprary char
static bool rx_ready=0;														//UART flag
static uint8_t rx_buf_pos=0;
static char rx_buf[16];
static bool alarm = 0;														//indicator

void Play_Sound(uint32_t freq, int delayms);
void Sound_OK(void);
void Sound_BAD(void);

//UART interupt procedure
void UART0_IRQHandler()
{
	if(UART0->S1 & UART0_S1_RDRF_MASK)
	{		
		temp=UART0->D;
		if (!rx_ready){
			if(temp!=0xa){  														//eol char
				rx_buf[rx_buf_pos] = temp;
				rx_buf_pos++;
		
			}else{    																	//if eol
				rx_ready = 1;
			}
		}
	}
}
//SENSOR interupt procedure
void PORTB_IRQHandler(void){
	if (mState == GUARD){														//only if in GUARD state
		if( PORTB->ISFR & (1 << DOORS) ){
			PORTB->PCR[DOORS]|= PORT_PCR_ISF_MASK;
			alarm = 1;
		}else if (PORTB->ISFR & (1 << MOVE)){
			PORTB->PCR[MOVE]|= PORT_PCR_ISF_MASK;
			alarm = 1;
		}
	}
	NVIC_ClearPendingIRQ(PORTB_IRQn);
}
//PIT interupt procedure  -- buttons
void PIT_IRQHandler() {

	if (PIT->CHANNEL[0].TFLG & PIT_TFLG_TIF_MASK) {
		if (!pass_ready){
			machineHandle(&buttonstruct);
			if(buttonstruct.value > 0 && buttonstruct.value <= 16 && !button_hold){
				buff[counter++]=buttonstruct.value;
				button_hold=1;														//button could be hold
				if (counter == passlen){ pass_ready=1; counter = 0;}
			}else if (buttonstruct.value == 0) button_hold = 0;
			buttonstruct.writeIt =0; 										//we used value
		}
		PIT->CHANNEL[0].TFLG &= PIT_TFLG_TIF_MASK;	
		
	} else if (PIT->CHANNEL[1].TFLG & PIT_TFLG_TIF_MASK) {
		
		PIT->CHANNEL[1].TFLG &= PIT_TFLG_TIF_MASK;
	}
	NVIC_ClearPendingIRQ(PIT_IRQn);
	
}


int main(void){
	LED_Init();
	Buttons_Init();																	//buttons
	pit_set(SystemCoreClock/100, 0);								//
	startPIT(0);																		//
	
	Alarm_Init();																		//INIT procedure
	mState = SetState(INIT);												//set machine's first state
			

	while(1){																				//endless loop
			
		__WFI();
		
		///////////////////////////////////INIT
		if (mState == INIT && pass_ready){ 						//set pass
			SetPass(buff);
			mState = SetState(WAITING);									//next state
			pass_ready=0; 															//data consumed
			Sound_OK();
		}
		
		////////////////////////////////WAITING
		if (mState == WAITING && pass_ready){ 
			if (CheckPass(buff)) {mState = SetState(ESCAPE); 	Sound_OK();}
			else 								{ mState = SetState(WAITING); Sound_BAD();}
			pass_ready=0; 															//data consumed
		}
		
		///////////////////////////////ESCAPE
		if (mState == ESCAPE){
			for (uint16_t k =0; k<ESCAPE_TIME; k++){
				delay_ms(1000);
				if (pass_ready){
					if (CheckPass(buff)) {mState = SetState(WAITING); k = ESCAPE_TIME;}  //to exit for loop imiediately
					else  mState = SetState(ESCAPE);
					pass_ready=0; 													//data consumed
				}
			}
			if (mState == ESCAPE) mState = SetState(GUARD);
		}
		
		///////////////////////////////GUARD
		if (mState == GUARD && alarm){
			mState=SetState(DEFUZE);
			alarm = 0;
		}
		
		//////////////////////////////DEFUZE
		if (mState == DEFUZE){
			for (uint16_t k =0; k<DEFUZE_TIME; k++){
				delay_ms(1000);
				if (pass_ready){
					if (CheckPass(buff)) {mState = SetState(WAITING); k = DEFUZE_TIME;}  //to exit for loop imiediately
					else  mState = SetState(DEFUZE);
					pass_ready=0; 												//data consumed
				}
			}
			if (mState == DEFUZE) mState = SetState(ALARM);
		}
		
		////////////////////////////////ALARM
		if (mState == ALARM && pass_ready){ 				//turn on code
			if (CheckPass(buff)) mState = SetState(WAITING);
			else  mState = SetState(ALARM);
			pass_ready=0; 														//data consumed
		}
		
		///////////////////////////////ESP information
		if (rx_ready){  
				rx_buf_pos = 0;
				if (rx_buf[0] == 'A') mState = SetState(ALARM);//information filter
				else if (rx_buf[0] == 'I') mState = SetState(INIT);
				else if (rx_buf[0] == 'O'){
					if (rx_buf[1] == 'n') mState = SetState(GUARD);
					else if (rx_buf[1] == 'f') mState = SetState(WAITING);}
				rx_ready = 0;
		}
	}
}


//////////////////FUNCTIONS

/**
 * @brief Play custom sound
 * @param Sound freq and sound duration 
 */
void Play_Sound(uint32_t freq, int delayms){
	Alarm_Start(freq);
	delay_ms(delayms);
	Alarm_Stop();
}

/**
 * @brief Custom sound1
 */
void Sound_OK(void){
	Play_Sound(1000, 200);
	delay_ms(100);
	Play_Sound(2000, 400);
}
	
/**
 * @brief Custom sound1
 */
void Sound_BAD(void){
	Play_Sound(500, 200);
	delay_ms(100);
	Play_Sound(200, 400);
}	
