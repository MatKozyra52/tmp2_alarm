/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file pit.c
 * @author MatKozyra
 * @date Dec 2020
 * @brief library for PIT functionality
 * @ver 0.1
 */

#include "../inc/pit.h"




void pit_set(uint32_t period, uint8_t channel) {

	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;

	PIT->MCR &= ~PIT_MCR_MDIS_MASK;  //PIT enabled
	PIT->MCR |= PIT_MCR_FRZ_MASK;  //timer stoped in debug
	
	PIT->CHANNEL[channel].LDVAL = PIT_LDVAL_TSV(period);
	
	PIT->CHANNEL[channel].TCTRL &= PIT_TCTRL_CHN_MASK; // No chaining
	
	PIT->CHANNEL[channel].TCTRL |= PIT_TCTRL_TIE_MASK; // Generate interrupts


	NVIC_ClearPendingIRQ(PIT_IRQn); 
	NVIC_EnableIRQ(PIT_IRQn);	

}


void startPIT(uint8_t channel) {
	PIT->CHANNEL[channel].TCTRL |= PIT_TCTRL_TEN_MASK;
}

void stopPIT(uint8_t channel) {
	PIT->CHANNEL[channel].TCTRL &= ~PIT_TCTRL_TEN_MASK;
}


