/******************************************************************************
 * This file is a part of the SM2 Course Homework                               *                                                 *
 ******************************************************************************/

/**
 * @file button_matrix.c
 * @author Kozyra
 * @date Oct 2020
 * @brief library .c file
 * @connecting R1-R4 -> PTA8-PTA5;   C1-C4 -> PTA12-PTA9
 * @ver 1.1
 * @update interrupt functionality
 */
 
#include "../inc/button_matrix.h"

#ifdef FRDM_KL05Z

# define COL_N 4
# define ROW_N 4
# define ERROR_VALUE 99

	
/******************************************************************************\
* Private definitions
\******************************************************************************/
/*Collumns*/
	static PinStruct_Type column[COL_N] = {
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 12},
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 11},
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 10},
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 9}
	};
	
/*Rows*/
	static PinStruct_Type row[ROW_N] = {
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 8},
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 7},
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 6},
		{PTA, PORTA, SIM_SCGC5_PORTA_MASK, 5}
	};
#endif /* FRDM_KL05Z */
	

/******************************************************************************\
* Private prototypes
\******************************************************************************/	
uint8_t Get_Sample(void);



void Buttons_Init(){
	
	 for(int i = 0; i < COL_N; i++) {
    SIM->SCGC5 |= column[i].clk_mask;              															/* connect CLOCK to port */
	  column[i].port->PCR[column[i].pin] |= PORT_PCR_MUX(1UL); 										/* set MUX to GPIO */
		column[i].port->PCR[column[i].pin] |= PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; 	/* Enable pull-up resitor on Pin*/

  }

	for(int i = 0; i < ROW_N; i++) {
    SIM->SCGC5 |= row[i].clk_mask;              					/* connect CLOCK to port */
	  row[i].port->PCR[row[i].pin] |= PORT_PCR_MUX(1UL); 		/* set MUX to GPIO */
	  row[i].gpio->PSOR |= MASK(row[i].pin);            		/* OFF as default  */
  }
}

void machineHandle(ButtonsStruct* local){
	if (!(local->writeIt)){																										 /* If we used value */
		if ((local->value == Get_Sample()) && local->value != local->prevValue){ /* Copare with previous sample and test if it changed from last time(holding button) */
			local->prevValue = local->value;
			local->writeIt = 1;
		}
		else local->value = Get_Sample();
	}
}


/**
 * @brief Single sample without debouncing
 */
uint8_t Get_Sample(void){
	int collStatus[COL_N]={0,0,0,0};
	for(uint8_t k = 0; k<ROW_N; k++){
		row[k].gpio->PDDR |= MASK(row[k].pin);								/* set as OUTPUT   */
		row[k].gpio->PCOR |= MASK(row[k].pin);								/* One active row per cycle */
		for(uint8_t m = 0; m<COL_N; m++){
			collStatus[COL_N-k-1] += (PTA->PDIR & (1<<column[m].pin))>>(column[m].pin-m); /* Store 4 inputs values in single variable */
		}
		row[k].gpio->PSOR |= MASK(row[k].pin);
		row[k].gpio->PDDR ^=MASK(row[k].pin);									/* set as INPUT because we don't use now */
	}
	uint8_t output=0;
	for(uint8_t c = 0; c<COL_N; c++){
		for(uint8_t r = 0; r<COL_N; r++){
			if(((collStatus[c]>>(COL_N-r-1)) & 1)==0) {					/* We are looking for '0' in our storage */
				if (!output) output=4*c +(r+1);										
				else output = ERROR_VALUE;												/* If there is many buttons pressed */
			}
		}
	}
	return output;
}


