/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file KL05Zutil.c
 * @author MatKozyra
 * @date Dec 2020
 * @brief definition for header
 * @ver 0.1
 */
 
#include "../inc/KL05Zutil.h"

/**
 * @brief Delay in ms
 */
void delay_ms( int n) {
	volatile int i;
	volatile int j;
	for( i = 0 ; i < n; i++)
	for(j = 0; j < 3500; j++) {}
}

/**
 * @brief Delay in us
 */
void delay_us( int n) {
	volatile int i;
	volatile int j;
	for( i = 0 ; i < n; i++)
	for(j = 0; j < 3; j++) {}
}
