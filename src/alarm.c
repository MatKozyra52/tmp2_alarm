/******************************************************************************
 * This file is a part of the Sysytem Microprocessor Project(C).            *
 ******************************************************************************/

/**
 * @file alarm.c
 * @author MatKozyra
 * @date Dec 2020
 * @brief main library for Alarm project
 * @ver 0.1
 */
 

#include "../inc/alarm.h"


static bool alarm_sound = 0;
static int counter = 0;
static char espinfo [30] = {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'} ;

//Connected sensors
static PinStruct_Type sensor[INPUT_SENSOR] = {
		{PTB, PORTB, SIM_SCGC5_PORTB_MASK, DOORS},
		{PTB, PORTB, SIM_SCGC5_PORTB_MASK, MOVE}	
	};

//SysTick interupt procedure
SysTick_Handler(){
	PTB->PTOR |= 1<<BUZZER_PIN;
	
	if (alarm_sound){    								//special alarm procedure for changing tone in systick
		if (counter == 3000) Alarm_Start(1000);
		else if (counter == 4000){
			Alarm_Start(3000);
			counter = 0;
		}
		counter ++;
	}
}

/////////////Internal function

/**
 * @brief Enable/Disable Sensor interupts
 */
void Security_ON(void);
void Security_OFF(void);

///////////////////////////////

void Alarm_Init(void){
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	PORTB->PCR[BUZZER_PIN] =PORT_PCR_MUX(1);
	PTB->PDDR |= 1<<BUZZER_PIN;
	alarm_sound = 0;
	counter = 0;
	UART_Init(115200); 										//BR = 115200
	UART_Print("System Init");  							//Information for ESP
	
	for(int i = 0; i < INPUT_SENSOR; i++) {
    SIM->SCGC5 |= sensor[i].clk_mask;        				// connect CLOCK to port
	  sensor[i].port->PCR[sensor[i].pin] = PORT_PCR_MUX(1UL); 			//set MUX to GPIO 
	  sensor[i].port->PCR[sensor[i].pin] |= PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; 	//pull up
  }
}


void Alarm_Start(uint32_t freq){
	SysTick_Config(SystemCoreClock/freq);
}

void Alarm_Stop(void){
	SysTick->CTRL  &= ~SysTick_CTRL_TICKINT_Msk;
	SysTick->CTRL  &= ~SysTick_CTRL_ENABLE_Msk; 
	PTB->PCOR |= 1<<BUZZER_PIN; 
	alarm_sound = 0;
}


Alarm_State SetState(Alarm_State state){
	LED_Ctrl(LED_BLUE, LED_OFF);
	LED_Ctrl(LED_GREEN, LED_OFF);
	LED_Ctrl(LED_RED, LED_OFF);
	Alarm_Stop();
	Security_OFF();
	switch(state){
		case INIT:
			LED_Ctrl(LED_BLUE, LED_ON);
			return INIT;
		case WAITING:
			LED_Ctrl(LED_GREEN, LED_ON);
			while(!(UART0->S1 & UART0_S1_TDRE_MASK)){};
			UART_Print("Security system OFF");
			return WAITING;
		case ESCAPE:
			LED_Ctrl(LED_RED, LED_ON);
			return ESCAPE;
		case GUARD:
			UART_Print("Security system ON");
			LED_Ctrl(LED_RED, LED_ON);
			LED_Ctrl(LED_GREEN, LED_ON);
			Security_ON();
			return GUARD;
		case DEFUZE:
			LED_Ctrl(LED_RED, LED_ON);
			return DEFUZE;
		case ALARM:
			UART_Print("Alarm");
			LED_Ctrl(LED_RED, LED_ON);
			alarm_sound=1;
			Alarm_Start(10000);
			return ALARM;
	}
}

bool CheckPass(uint8_t *buff){
	bool correct = 1;
	for (uint8_t k = 0 ; k<passlen; k++){
		if(pass[k]!=buff[k]) correct = 0;
	}
	if (!correct) UART_Print("Incorrect password");
	return correct;
}


void SetPass(uint8_t *buff) {
	for(uint8_t k = 0; k<passlen; k++){
		pass[k]=buff[k];
	}
	sprintf(espinfo,"New password : %d, %d, %d, %d", buff[0],buff[1],buff[2], buff[3] );
	UART_Println(espinfo);
}



void Security_ON(void){  										//enable interupt
	for(int i = 0; i < INPUT_SENSOR; i++) {
		sensor[i].port->PCR[sensor[i].pin] |= PORT_PCR_IRQC(0x09);
		NVIC_ClearPendingIRQ(PORTB_IRQn); 
		NVIC_EnableIRQ(PORTB_IRQn);	
	}
}
	
void Security_OFF(void){  										//disable interupt
	for(int i = 0; i < INPUT_SENSOR; i++) {
		sensor[i].port->PCR[sensor[i].pin] |= PORT_PCR_IRQC(0);
		NVIC_ClearPendingIRQ(PORTB_IRQn); 
		NVIC_DisableIRQ(PORTB_IRQn);	
	}
}
