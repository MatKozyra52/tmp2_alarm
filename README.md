# System antywłamaniowy rozbudowany o komunikacje Wifi
Głównym założeniem projektu było stworzenie systemu antywłamaniowego a następnie rozbudowanie działającego już układu o dodatkową funkcjonalność poprzez wprowadzenie łączności WiFi. Zaletą takiego podejścia jest pełna niezależność podstawowej wersji alarmu od sieci Wifi co oznacza że może w pełni funkcjonować po utracie połączenia.   
Do realizacji projektu wykorzystałem płytkę rozwojową FRDM-KL05Z z procesorem Cortex-M0+. Program podstawowej wersji alarmu został napisany w C. Programując moduł ESP8266 odpowiadający za komunikację wykorzystałem język C++, oraz HTML5+JS do stworzenia strony internetowej.  
Projekt zawiera autorskie biblioteki jak również kod analizowany na zajęciach z Technik Mikroprocesorowych 2.   

## Skrócony opis działania  
Po włączeniu zasilania układ pozwala ustawić hasło, które zostanie zapamiętane. Hasło to jest wymagane aby uzbroić system, rozbroić go, jak również wyłączyć sygnał dźwiękowy gdy zostanie już uruchomiony. Detekcja intruza odbywa się poprzez czujnik otwarcia drzwi/ okna, oraz czujnik ruchu.   
Moduł ESP8266 pozwala połączyć się z układem poprzez sieć lokalną. Poprzez przeglądarkę jesteśmy w stanie na bieżąco obserwować historię wydarzeń (ustawione hasło, zmiana trybu, detekcja, alarm), jak również zdalnie kontrolować stan systemu (uzbrojenie, dezaktywacja, reset hasła, alarm).


## Komponenty/ Elementy składowe
 - KL05Z - logika układu
 - Klawiatura 4x4 - ustanawianie hasła, uzbrajanie alarmu, dezaktywacja
 - Głośnik/ Buzzer bez generatora - alarm
 - Czujnik ruchu PIR - system detekcji
 - Magnetyczny czujnik otwarcia drzwi/ okna - system detekcji
 - (opcjonalne - zalecane) ESP8266 - moduł komunikacji Wifi
 - (opcjonalne) LCD z interfejsem I2C - może posłużyć do debuggowania układu w przypadku braku połączenia Wifi
 - (opcjonalne) Zewnętrzny układ zasilania 5V oraz 3.3V

### Schemat urzadzenia
 ![schematic](images/sch.png)

## Zasada działania - stacja główna  
Program główny został stworzony na wzór maszyny stanów, co oznacza że układ trwa w pewnym stanie oczekując na przerwanie, bądź ciąg przerwań które doprowadzą do zmiany stanu. Zastosowanie takiego podejścia pozwala łatwo zobrazować działanie układu.  
Po włączeniu zasilania układ znajduje się w stanie **INIT** - odpowiada to zapaleniu sie diody LED w kolorze niebieskim - układ oczekuje na ustanowienie hasła. Poprzez przerwania licznika PIT gromadzi kolejne liczby tak długo aż nie zdobędzie pełnego hasła - domyślnie są to 4 liczby. Po zebraniu wartości hasło jest zapisywane, a układ przechodzi do następnego stanu.   
**WAITING** to stan w którym układ jest nieuzbrojony i czeka na aktywacje poprzez ustanowione wcześniej hasło - identyfikowany jest poprzez zielony kolor diody LED. Podobnie jak w przypadku stanu INIT układ w przerwaniach licznika PIT zbiera dane z klawiatury po czym porównuje je z ustanowionym hasłem. Jeżeli hasło jest poprawne następuje zmiana trybu, w przeciwnym razie nadal czekamy na poprawną kombinacje.   
Następnym stanem jest **ESCAPE** (czerwony kolor led) należy opuścić pomieszczenie, bądź wprowadzić hasło dezaktywujące. Czas po którym urządzenie przejdzie do następnego trybu podlega modyfikacji w programie.    
Stan **GUARD** -pomarańczowy kolor LED- układ monitoruje teren oczekując na przerwania od sensorów. Wykryty ruch, bądź przerwanie od czujnika przypiętego do drzwi powoduje natychmiastowe przejście do następnego stanu.    
**DEFUZE** - czerwona dioda LED - układ czeka na hasło i jednocześnie odlicza predefiniowany czas (Defuze time). Jest to okazja do dezaktywowania urządzenia zanim alarm się uruchomi.    
Jeżeli nie znamy hasła, bądź nie wprowadzimy go w odpowiednim czasie - nastąpi **ALARM** - czerwony kolor LED. Jest to stan, któremu towarzyszy nieprzerwana sygnalizacja dźwiękowa o zmiennym tonie. Układ oczekuje na zdefiniowane hasło w celu dezaktywacji.    

Działanie można zobrazować poprzez następujący schemat:  
 ![states](images/states.png) 

Edycja kodu pozwala zmodyfikować czas stanu ESCAPE, oraz DEFUZE, a także siłę hasła poprzez zmianę jego długości - domyślnie są to 4 liczby co daje ponad 65.5tys kombinacji.    

## Moduł Wifi 
### Funkcjonalność
Połączenie urządzenia do sieci lokalnej znacznie rozszerza jego możliwości. Zyskujemy pełną kontrolę nad stanem urządzenia, jak również możemy obserwować historię wydarzeń - Changelog   
     
Moduł ESP tworzy serwer, który umożliwia sterowanie urządzeniem z poziomu przejrzystej strony internetowej    
       
 ![web](images/web.png) 

 Lista funkcji widocznych z poziomu strony:
 - ON -> uzbraja urządzenie które od tego momentu monitoruje pomieszczenie - stan GUARD  
 - OFF -> dezaktywuje system - stan WAITING
 - ALARM -> zdalne uruchomienie alarmu - stan ALARM 
 - Reset System -> pozwala na ustawienie nowego hasła - INIT
 - Raw data -> przenosi do podstrony na której wyświetlana jest wyłącznie historia zdarzeń w niesformatowanej formie
 - CLEAR -> oczyszczenie wewnętrznej pamięci modułu ESP, w której przetrzymywana jest historia zdarzeń
 - Changelog -> na bieżąco aktualizowana historia zdarzeń oznakowana datą i godzina wystąpienia  


### Zasada działania - kod  
ESP8266 jest układem programowalnym, co wiąże sie z koniecznością stworzenia osobnego oprogramowania. Do tego celu użyłem gotowych bibliotek modułu napisanych w C++. Jako środowisko wykorzystałem Visual Studio Code z zainstalowaną wtyczką PlatformIO, ale można również z powodzeniem wykorzystać Arduino IDE.  
Poza zbiorem instrukcji konieczne jest również stworzenie responsywnej strony internetowej. Moduł ESP pozwala przechowywać dane w wewnętrznej pamięci Flash i odwoływać się do nich z poziomu głównego programu.   
Po uruchomieniu zasilania moduł niezwłocznie podejmuje próbę połącznia z zdefiniowana siecią. Inicjalizowany jest również UART, jak również dane niezbędne do komunikacji poprzez TCP oraz UDP (przy pomocy TCP przesyłane będą informacje do klienta, natomiast UDP posłuży do komunikacji z serwerem NTP - Network Time Protocol). Następnie moduł przechodzi do pętli, w której to oczekuje na połącznie klienta, bądź na transmisję UART. W przypadku otrzymania danych poprzez UART następuje wysłanie zapytania do serwera NTP. W odpowiedzi otrzymujemy aktualny czas, który zostaje zapisany wraz z danymi w pliku tekstowym w wewnętrznej pamięci Flash. Strona internetowa widziana przez użytkownika powstaje w wyniku odtworzenia wewnętrznego plik HTML, który przy pomocy Java Script eksportuje zapisane dane.   
Stworzony kod jest więc uniwersalnym systemem logującym dane otrzymane poprzez UART.   
Komunikacja w drugą stronę - od użytkownika do urządzenia - zrealizowana jest poprzez system obsługi zapytań HTML. Uzytkownik wysyła zapytanie do serwera, który odsyła stronę i jednocześnie generuje dane dla systemu poprzez UART. Dane te są odbierane przez płytkę KL05Z i odpowiednio interpretowane. Ostatecznie mamy więc dwa odrębne systemy, które współgrają poprzez wzajemną komunikacje.



## Rezultat 

### Zdjęcia  
![image](https://drive.google.com/uc?export=view&id=1ZRUF8DUjwwcx2qXEHKYeMU_Kn8IfO8_c)   
![image](https://drive.google.com/uc?export=view&id=1E-3dndSdDnjmfAGKOpJUGM2rtTV_TkVe)      

### Video  
[YouTube](https://youtu.be/SbLfzCcKy24)     

